package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import org.springframework.stereotype.Component;

@ExportAsDevService
@Component
public class PublicDevService implements Service {
    @Override
    public void a() {
    }
}
