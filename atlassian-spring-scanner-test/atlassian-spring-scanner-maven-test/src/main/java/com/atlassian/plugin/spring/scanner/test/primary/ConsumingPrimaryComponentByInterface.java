package com.atlassian.plugin.spring.scanner.test.primary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static java.lang.String.format;

@Component
public class ConsumingPrimaryComponentByInterface {

    @Autowired
    ConsumingPrimaryComponentByInterface(ComponentInterfaceWithAPrimaryImplementation component) {
        if (!(component instanceof PrimaryImplementation)) {
            throw new IllegalStateException(format("The bean autowired for ComponentInterfaceWithAPrimaryImplementation was not an instance of PrimaryImplementation, instead it was %s", component.getClass().getName()));
        }
    }

}
