package com.atlassian.plugin.spring.scanner.test.notscanned;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.external.notannotated.NotAnnotatedExternalJarClass;
import com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceTwoExportedFromAnotherPlugin;

/**
 * SCANNER-52 means that we no longer need @Scanned but it will still be considered in import terms
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
public class NotAnnotatedAsAComponent {
    private final ServiceTwoExportedFromAnotherPlugin serviceTwo;
    private final NotAnnotatedExternalJarClass classpathComponent;

    public NotAnnotatedAsAComponent(@ComponentImport ServiceTwoExportedFromAnotherPlugin serviceTwo,
                @ClasspathComponent NotAnnotatedExternalJarClass classpathComponent) {
        this.serviceTwo = serviceTwo;
        this.classpathComponent = classpathComponent;
    }
}
