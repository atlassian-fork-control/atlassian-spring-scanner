package com.atlassian.plugin.spring.scanner.test.primary;

import org.springframework.stereotype.Component;

/**
 * This is an implementation for the {@link ComponentInterfaceWithAPrimaryImplementation} interface, but it is not the
 * primary implementation.
 */
@Component
class BaseImplementation implements ComponentInterfaceWithAPrimaryImplementation {
}
