package com.atlassian.plugin.spring.scanner.test.exported;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

@ExportAsService(ExposedAsAServiceComponentInterfaceThree.class)
@Component
public class ExposedAsAServiceComponentWithSpecifiedInterface implements ExposedAsAServiceComponentInterfaceThree, DisposableBean {
    @Override
    public void destroy() {
    }

    @Override
    public void doMoreStuff() {
    }
}
