package it.allproducts;

import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.test.servlet.CallCustomModuleTypeServlet;
import it.perproduct.AbstractInProductTest;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Integration test for the use of {@link ModuleType}.
 */
public class TestCustomModuleType extends AbstractInProductTest {
    /**
     * @see CallCustomModuleTypeServlet
     */
    private static final String CALL_CUSTOM_MODULE_URL = BASEURL + "/plugins/servlet/call-custom-module-type";

    /**
     * Test that the {@code <basic/>} element in atlassian-plugin.xml has created an instance of
     * {@link com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleDescriptor}.
     */
    @Test
    public void testCustomModuleTypeIsInstalled() throws Exception {
        assertEquals("Basic module is working", getUrl(CALL_CUSTOM_MODULE_URL));
    }
}
